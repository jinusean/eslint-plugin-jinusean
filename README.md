# eslint-plugin-jinusean

Jinusean&#39;s not so secret home-made linting recipes

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-jinusean`:

```
$ npm install --save-dev eslint-plugin-jinusean
```

**Note:** This plugin works in conjunction with `eslint-plugin-vue` and is required to extend `plugin:jinusean/recommended` 

## Usage

Add `jinusean` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "jinusean"
    ],
    "extends": [
      "plugin:jinusean/recommended"
    ],
    "rules": {
      // override rules here
      // "jinusean/min-attributes-per-line": "error"
    }
}
```

## Supported Rules

* `min-attributes-per-line`





