/**
 * @fileoverview Jinusean&#39;s not so secret home-made linting recipes
 * @author James C. Lee
 */
'use strict'

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------


const requireIndex = require('requireindex')

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------
module.exports = {
  rules: requireIndex(__dirname + '/rules'),
  configs: {
    recommended: require('./configs/recommended')
  }
}