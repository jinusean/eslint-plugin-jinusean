/**
 * @fileoverview Minimum attributes before new line
 * @author James C. lee
 */
'use strict'

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
module.exports = {
  meta: {
    docs: {
      description: 'Minimum attributes before new line',
      category: 'Fill me in',
      recommended: true
    },
    fixable: 'whitespace',
    schema: [
      {
        type: 'object',
        properties: {
          count: {
            type: 'number',
            minimum: 1
          },
          requireNewLine: {
            type: 'boolean'
          }
        },
        additionalProperties: false
      }
    ]
  },

  create: function (context) {
    const defaults = {
      minAttributesCount: 1,
      requireNewLine: true
    }
    const minAttributesCount = context.minAttributesCount || defaults.minAttributesCount
    const requireNewLine = context.requireNewLine || defaults.requireNewLine
    const template = context.parserServices.getTemplateBodyTokenStore && context.parserServices.getTemplateBodyTokenStore()

    function getPropData(attribute) {
      let propType = 'Attribute'
      let propName = attribute.key.name

      if (attribute.directive &&
        attribute.key.name === 'bind' &&
        attribute.key.argument) {
        propType = 'Binding'
        propName = attribute.key.raw.argument
      } else if (attribute.directive && attribute.key.name === 'on') {
        propType = 'Event'
        propName = attribute.key.raw.argument
      } else if (attribute.directive) {
        propType = 'Directive'
      }

      return {propType, propName}
    }

    return context.parserServices.defineTemplateBodyVisitor({
      VStartTag(node) {
        const numberOfAttributes = node.attributes.length
        const isSingleLine = node.loc.start.line === node.loc.end.line

        if (numberOfAttributes < minAttributesCount) {
          return
        }

        let currentLine = node.loc.start.line
        let currentLineAttributeCount = 0

        node.attributes.forEach((attribute, index) => {
          const isSameLine = currentLine === attribute.loc.start.line

          if (isSameLine) {
            currentLineAttributeCount++
            // Current line exceeds minAttribute and there is no new line
            if (requireNewLine && currentLineAttributeCount > minAttributesCount) {
              return context.report({
                node: attribute,
                loc: attribute.loc,
                message: '{{propType}} "{{propName}}" should be on a new line.',
                data: getPropData(attribute),
                fix(fixer) {
                  // Find the closest token before the current prop
                  // that is not a white space
                  const prevToken = template.getTokenBefore(attribute, {
                    filter: (token) => token.type !== 'HTMLWhitespace'
                  })

                  const range = [prevToken.range[1], attribute.range[0]]
                  return fixer.replaceTextRange(range, '\n')
                }
              })
            }

            return
          }

          const previousLineAttributeCount = currentLineAttributeCount
          currentLineAttributeCount = 1
          currentLine = attribute.loc.start.line

          // Is now new line but required number of attributes has not yet been met
          if (previousLineAttributeCount < minAttributesCount) {
            return context.report({
              node: attribute,
              loc: attribute.loc,
              message: '{{propType}} "{{propName}}" should be on previous line.',
              data: getPropData(attribute),
              fix(fixer) {
                const prevToken = template.getTokenBefore(attribute, {
                  filter: (token) => token.type !== 'HTMLWhitespace'
                })

                const range = [prevToken.range[1], attribute.range[0]]
                return fixer.replaceTextRange(range, ' ')
              }
            })
          }
        })
      }
    })
  }
}
