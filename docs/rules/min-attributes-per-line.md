# Minimum attributes before new line (min-attributes-per-line)


- This rule is included in `"plugin:jinusean/recommended"` and `"plugin:vue/recommended"`.
- The `--fix` option on the [command line](https://eslint.org/docs/user-guide/command-line-interface#fixing-problems) can automatically fix some of the problems reported by this rule.

Require a minimum number of attributes before going to the next line to improve readability.

By default, the minimum attribute is 1, and a newline is required after.

## Rule Example

Vue style-guide suggests this
```html
<parent-component
    foo="a"
    bar="b"
    baz="c">
    <child-component sibling="a"/>
</parent-component>
```


This rule suggests this
```html
<parent-component foo="a"
                  bar="b"
                  baz="c">
    <child-component sibling="a"/>
</parent-component>>
```




## Rule Details

This rule aims to enforce a number of attributes per line in templates.
It checks all the elements in a template and verifies that an attribute goes to a new line only if the previous line meets the required minimum number.
An attribute is considered to be in a new line when there is a line break between two attributes.

Examples of **incorrect** code for this rule:

```html
<my-component lorem="1" ipsum="2"/>  // Ipsum should be on new line 

<my-component                        // lorem should be on previous line
  lorem="1" ipsum="2"
/>

<my-component
  lorem="1" ipsum="2"               // ipsum should be on a new line
  dolor="3"
/>
```

Examples of **correct** code for this rule:

```html
<my-omponent lorem="1"/>

<my-omponent lorem="1"
             ipsum="2"
/>
```

### Options

```json
{
  "jinusean/min-attributes-per-line": [2, {
    "minAttributesCount": 2,
    "requireNewLine": true   
  }]
}
```

## When Not To Use It

If you do not want an attribute on the same line as the opening tag.
****
