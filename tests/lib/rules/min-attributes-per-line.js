/**
 * @fileoverview Minimum attributes before new line
 * @author James C. lee
 */
'use strict'

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require('../../../lib/rules/min-attributes-per-line'),

  RuleTester = require('eslint').RuleTester


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parser: 'vue-eslint-parser',
  parserOptions: { ecmaVersion: 2015 }
})

ruleTester.run('min-attributes-per-line', rule, {

  valid: [
    {
      code: '<template><my-component class="my-class"/></template>'
    },
    {
      code: '<template><component my="my-class"></component></template>'
    },
    {
      code: `<template>
                 <component class="my-class"
                            style="height: 200px;"/>
            </template>`
    },
    {
      code: `<template><div class="progress-bar color-1"
                            role="progressbar"
                            aria-valuenow="75"
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style="width:75%"/>
            </template>`
    }
  ],

  invalid: [
    {
      code: `<template><my-component\nclass="my-class"/></template>`,
      output: `<template><my-component class="my-class"/></template>`,
      errors: ['Attribute "class" should be on previous line.']
    },
    {
      code: `<template><my-component class="my-class" style="height: 200px;"/></template>`,
      output: '<template><my-component class="my-class"\nstyle="height: 200px;"/></template>',
      errors: ['Attribute "style" should be on a new line.']
    },
    {
      code: `<template><my-component class="my-class" style="height: 200px;" id="1"/></template>`,
      output: `<template><my-component class="my-class"\nstyle="height: 200px;"\nid="1"/></template>`,
      errors: ['Attribute "style" should be on a new line.', 'Attribute "id" should be on a new line.']
    }
  ]
})
